<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$MESS ['SETTINGS_PHR']     = "Настройки";
$MESS ['PARAMS_PHR']       = "Параметры";
$MESS ['IBLOCK_TYPE']      = "Тип инфоблока";
$MESS ['IBLOCK_ID']        = "Инфоблок";
