<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array                    $arParams
 * @var array                    $arResult
 * @global CMain                 $APPLICATION
 * @global CUser                 $USER
 * @global CDatabase             $DB
 * @var CBitrixComponentTemplate $this
 * @var string                   $templateName
 * @var string                   $templateFile
 * @var string                   $templateFolder
 * @var string                   $componentPath
 * @var CBitrixComponent         $component
 **/

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


/**
 * Class CSize4Placeholder
 */
class CSize4Placeholder extends CBitrixComponent
{
    public $arParams;
    public $arResult;
    /**
     * кешируемые ключи arResult
     *
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     *
     * @var array
     */
    protected $cacheAddon = array();

    /**
     * парамтеры постраничной навигации
     *
     * @var array
     */
    protected $navParams = array();

    /**
     * @param $arParams
     *
     * @return mixed
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams["CACHE_TYPE"]    = 'A';
        $arParams["CACHE_TIME"]    = isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000;
        $arParams["WIDTH"]         = intval($arParams["WIDTH"]);
        $arParams["HEIGHT"]        = intval($arParams["HEIGHT"]);
        $arParams["TEXT_ORIGINAL"] = isset($arParams["TEXT"]) ? urlencode($arParams["TEXT"]) : '';
        $arParams["TEXT"]          = isset($arParams["TEXT"]) ? '?text=' . urlencode($arParams["TEXT"]) : '';

        return $arParams;
    }

    /**
     * подключает языковые файлы
     */
    public function onIncludeComponentLang()
    {
        $this->includeComponentLang(basename(__FILE__));
        Loc::loadMessages(__FILE__);
    }

    /**
     * Выполняет логику работы компонента
     *
     * @return mixed
     */
    public function executeComponent()
    {
        try {
            if ($this->StartResultCache()) {
                $this->arResult["BG_COLOR"]   = self::random_html_color(true);
                $this->arResult["TEXT_COLOR"] = self::random_html_color();
                self::includeComponentTemplate();
            }

        } catch (Exception $e) {
            $this->abortDataCache();
            ShowError($e->getMessage());
        }
    }


    private function random_html_color($capacity = false)
    {
        if ($capacity)
            return sprintf('%02X%02X%02X', rand(0, 255), rand(0, 255), rand(0, 255));
        else
            return sprintf('%02X%02X%02X', rand(0, 100), rand(0, 100), rand(0, 100));
    }
}
