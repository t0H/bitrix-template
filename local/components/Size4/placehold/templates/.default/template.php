<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);
$frame = $this->createFrame($arParams["TEXT_ORIGINAL"])->begin("Loading...");
$frame->setAnimation(true);
?><img src="http://placehold.it/<?=$arParams["WIDTH"]?>x<?=$arParams["HEIGHT"]?>/<?= $arResult["BG_COLOR"]?>/<?= $arResult["TEXT_COLOR"]?><?=$arParams["TEXT"]?>" alt="<?=$arParams["TEXT_ORIGINAL"]?>"><? $frame->end();?>
