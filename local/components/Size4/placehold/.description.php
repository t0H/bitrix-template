<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
   "NAME" => Loc::getMessage("placehold_NAME"),
   "DESCRIPTION" => Loc::getMessage("placehold_DESCR"),
   "ICON" => "/images/icon.gif",
   "PATH" => array(
      "ID" => "placehold",
      "NAME" => "Заглушка"
   ),
   "CACHE_PATH" => "N"
);

#https://dev.1c-bitrix.ru/community/blogs/components2/133.php
