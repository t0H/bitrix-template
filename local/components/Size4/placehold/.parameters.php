<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Iblock;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);



$arComponentParameters = array(
    "GROUPS"     => array(
        "SETTINGS" => array(
            "NAME" => Loc::getMessage("SETTINGS_PHR"),
        ),
        "PARAMS"   => array(
            "NAME" => Loc::getMessage("PARAMS_PHR")
        ),
    ),
    "PARAMETERS" => array(

        "TEXT" => array(
            "NAME"    => "Текст",
            "TYPE"    => "STRING",
            "DEFAULT" => "",
        ),

        "WIDTH" => array(
            "NAME"    => "Ширина px",
            "TYPE"    => "STRING",
            "DEFAULT" => "200",
        ),
        "HEIGHT" => array(
            "NAME"    => "Высота px",
            "TYPE"    => "STRING",
            "DEFAULT" => "100",
        ),
    )
);
?>
