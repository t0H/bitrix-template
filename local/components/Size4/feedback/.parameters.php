<?
$arComponentParameters = array(
   "GROUPS" => array(
      "SETTINGS" => array(
         "NAME" => GetMessage("SETTINGS_PHR")
      ),
      "PARAMS" => array(
         "NAME" => GetMessage("PARAMS_PHR")
      ),
   ),
   "PARAMETERS" => array(
      "SET_TITLE" => array(),
      "CACHE_TIME" => array(),
      "VARIABLE_ALIASES" => array(
         "IBLOCK_ID" => array(
            "NAME" => GetMessage("CATALOG_ID_VARIABLE_PHR"),
         ),
         "SECTION_ID" => array(
            "NAME" => GetMessage("SECTION_ID_VARIABLE_PHR"),
         ),
      ),
      "SEF_MODE" => array(
         "list" => array(
            "NAME" => GetMessage("CATALOG_LIST_PATH_TEMPLATE_PHR"),
            "DEFAULT" => "index.php",
            "VARIABLES" => array()
         ),
         "section1" => array(
            "NAME" => GetMessage("SECTION_LIST_PATH_TEMPLATE_PHR"),
            "DEFAULT" => "#IBLOCK_ID#",
            "VARIABLES" => array("IBLOCK_ID")
         ),
         "section2" => array(
            "NAME" => GetMessage("SUB_SECTION_LIST_PATH_TEMPLATE_PHR"),
            "DEFAULT" => "#IBLOCK_ID#/#SECTION_ID#",
            "VARIABLES" => array("IBLOCK_ID", "SECTION_ID")
         ),
      ),
   )
);
?>