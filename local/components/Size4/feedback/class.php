<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array                    $arParams
 * @var array                    $arResult
 * @global CMain                 $APPLICATION
 * @global CUser                 $USER
 * @global CDatabase             $DB
 * @var CBitrixComponentTemplate $this
 * @var string                   $templateName
 * @var string                   $templateFile
 * @var string                   $templateFolder
 * @var string                   $componentPath
 * @var CBitrixComponent         $component
 **/

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class CT0hTest extends CBitrixComponent
{
    public function onPrepareComponentParams($arParams)
    {
        $result = array(
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "CACHE_TIME" => isset($arParams["CACHE_TIME"]) ? $arParams["CACHE_TIME"] : 36000000,
            "X"          => intval($arParams["X"]),
            "strMainID"  => 'popObject_' . randString()
        );

        return $result;
    }

    public function executeComponent()
    {
        global $APPLICATION;
        $requestAjax = isset($_REQUEST['AJAX_CALL']) && $_REQUEST['AJAX_CALL'] == 'Y';
        if ($requestAjax)
            $APPLICATION->RestartBuffer();

        if ($this->startResultCache()) {
        }
        #$this->arResult['TEST'] = Loc::getMessage("TEST");

        if (!$requestAjax) CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
        if ($requestAjax) {
            $this->setFrameMode(false);
            $this->IncludeComponentTemplate("ajax");
            define("PUBLIC_AJAX_MODE", true);
            require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_after.php");
            die();
        } else {
            $this->includeComponentTemplate();
        }

        #return parent::executeComponent();
        return $this->arResult["Y"];
    }
}
