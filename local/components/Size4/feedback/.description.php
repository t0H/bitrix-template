<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$arComponentDescription = array(
   "NAME" => Loc::getMessage("COMP_NAME"),
   "DESCRIPTION" => Loc::getMessage("COMP_DESCR"),
   #"ICON" => "/images/icon.gif",
   "PATH" => array(
      "ID" => "content",
      "CHILD" => array(
         "ID" => "catalog",
         "NAME" => "Пример компонента"
      )
   ),
   "AREA_BUTTONS" => array(
      array(
         'URL' => "javascript:alert('Это кнопка!!!');",
         'SRC' => '/images/button.jpg',
         'TITLE' => "Это кнопка!"
      ),
   ),
   "CACHE_PATH" => "N"
);

#https://dev.1c-bitrix.ru/community/blogs/components2/133.php