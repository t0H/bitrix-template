<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
$mainPage = (strpos($APPLICATION->GetCurPage(true), SITE_DIR . "index.php") === 0)? true : false;
$curPage  = $APPLICATION->GetCurPage(true);
?><html>
<head><?
$APPLICATION->ShowMeta("robots", false, true);
$APPLICATION->ShowMeta("keywords", false, true);
$APPLICATION->ShowMeta("description", false, true);
$APPLICATION->ShowCSS(true, true);

$APPLICATION->SetAdditionalCSS("/css/docs.css");

#CJSCore::Init(array('ajax'));

$APPLICATION->AddHeadScript("/js/libs/jquery.min.js");
$APPLICATION->AddHeadScript("/js/libs/bootstrap.min.js");
$APPLICATION->AddHeadScript("/js/libs/modernizr.min.js");
$APPLICATION->AddHeadScript("/js/script.js");
?>
<?
$APPLICATION->ShowHeadStrings();
$APPLICATION->ShowHeadScripts();
?>
<title><?$APPLICATION->ShowTitle()?></title>
</head>

<body>
<div id="panel">
    <? $APPLICATION->ShowPanel(); ?>
</div>
<h1><? $APPLICATION->ShowTitle("title", true); ?></h1>
