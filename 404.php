<?
if ($_SERVER['HTTP_X_FORWARDED_REQUEST'] != 'GET /404.php HTTP/1.1')
    include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
header('HTTP/1.1 404 Not Found');
header('Status: 404 Not Found');
#CHTTP::SetStatus("404 Not Found");
#@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$APPLICATION->SetTitle("Страница не найдена");
#$APPLICATION->AddChainItem("Страница не найдена",$APPLICATION->GetCurPage(false));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_after.php");
?>
<h1>Страница не найдена</h1>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
